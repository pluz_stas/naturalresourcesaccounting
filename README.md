**Thesis project "Natural resources accounting"**

The aim of the project is to solve the problem of accounting for natural resources.

## Start project

1. This project is using PostGIS and you should install postgresql and postgis libraries. 
2. Run pip install requirements.txt
3. Create db and change db credentials at settings.py/DATABASES  
4. Run ./manage.py migrate
5. Start project 
