"""naturalresourcesaccounting URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from resources import views
from resources.views import ResourceViewSet, ResourceTypeViewSet
from users.views import UserViewSet
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Natural resources accounting API')

router = DefaultRouter()

router.register(r'resources', ResourceViewSet)
router.register(r'users', UserViewSet)
router.register(r'resources_types', ResourceTypeViewSet)


urlpatterns = [
    path('docs/', schema_view),
    path('admin/', admin.site.urls),
    path('', views.MapView.as_view(), name="home"),
    path('grappelli/', include('grappelli.urls')),
    path('users/', include("users.urls")),
    path('resources/', include("resources.urls")),
    path("api/v1.0/", include(router.urls)),
]
