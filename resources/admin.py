from django.contrib import admin
from django.contrib.gis.admin import GeoModelAdmin, OSMGeoAdmin

from resources.models import Resource, ResourceType, Attribute, ResourcesAttributes


class ResourcesAttributesInline(admin.TabularInline):
    model = ResourcesAttributes
    extra = 1


@admin.register(Resource)
class ResourceAdmin(OSMGeoAdmin):
    inlines = (ResourcesAttributesInline,)
    list_display = ("id", "owner_name", "type")


@admin.register(Attribute)
class AttributeAdmin(admin.ModelAdmin):
    inlines = (ResourcesAttributesInline,)


@admin.register(ResourceType)
class ResourceTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "parent_type")


# class GeoReAdmin(GeoModelAdmin):
#     openlayers_url =