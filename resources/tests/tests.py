import time
from unittest.mock import MagicMock, Mock, patch

from django.test import TestCase
from rest_framework.reverse import reverse

from resources.tests.factories import UserFactory, ResourceFactory, ResourceTypeFactory
from resources.views import ResourceViewSet, ResourceTypeViewSet


class TestResourceModel(TestCase):

    def setUp(self):
        self.resource = ResourceFactory()

    def test_str_method(self):
        self.assertEqual(str(self.resource), f"Resource #{self.resource.id}")

class TestResourceTypeModel(TestCase):

    def setUp(self):
        self.resource_type = ResourceTypeFactory()


class TestResourceViewSet(TestCase):

    def test_get_queryset(self):
        instance = MagicMock()
        ResourceViewSet.get_queryset(instance)


    @patch("resources.views.ResourceViewSet.get_queryset")
    def test_list(self, get_queryset):
        url = reverse("resource-list")
        response = self.client.get(url)
        get_queryset.assert_called_once()
        self.assertEqual(response.data, {'type': 'FeatureCollection', 'crs': {'type': 'name', 'properties': {'name': 'EPSG:4326'}}, 'features': []})


class TestMapView(TestCase):

      def test_get_map(self):
          url = reverse("map")
          response = self.client.get(url)
          self.assertEqual(response.status_code, 200)

