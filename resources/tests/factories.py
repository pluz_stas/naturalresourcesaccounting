import factory
from factory import PostGenerationMethodCall, SubFactory, fuzzy
from factory.django import DjangoModelFactory

from resources.models import Resource, ResourceType
from users.tests.factories import UserFactory


class ResourceTypeFactory(DjangoModelFactory):
    class Meta:
        model = ResourceType
    name = factory.Faker("name")
    description = fuzzy.FuzzyText(length=100)

class ResourceFactory(DjangoModelFactory):
    class Meta:
        model = Resource

    user = SubFactory(UserFactory)
    type = SubFactory(ResourceTypeFactory)
