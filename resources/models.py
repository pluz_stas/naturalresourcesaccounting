# from django.db import models
from django.contrib.gis.db import models

from users.models import User


class ResourceType(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=255)
    parent_type = models.OneToOneField('self', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name


class Attribute(models.Model):
    resource_type = models.ManyToManyField(ResourceType, related_name="attributes_resources_types")
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Resource(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    location_point = models.PointField(null=True, blank=True)
    location_polygon = models.PolygonField(null=True, blank=True)
    owner_name = models.CharField(max_length=100, null=True, blank=True)
    type = models.ForeignKey(ResourceType, on_delete=models.CASCADE, null=True)
    attributes = models.ManyToManyField(Attribute, related_name="resources_attributes", through="ResourcesAttributes")

    def save(self, *args, **kwargs):
        if self.user and self.owner_name is None:
            self.owner_name = User.get_full_name(self.user)
        super().save(*args, **kwargs)

    def __str__(self):
        return f"Resource #{self.id}"


class ResourcesAttributes(models.Model):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE)
    value = models.CharField(max_length=256)
