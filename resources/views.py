from itertools import chain

from django.core.serializers import serialize
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView
from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet
import json

from naturalresourcesaccounting import settings
from resources.models import Resource, ResourceType
from resources.serializers import ResourceSerializer, ResourceTypeSerializer


class MapView(TemplateView):
    template_name = "map.html"


class ResourceViewSet(ModelViewSet):
    serializer_class = ResourceSerializer
    queryset = Resource.objects.all()

    def get_queryset(self):
        queryset = self.queryset
        owner_name = self.request.query_params.get('owner_name', None)
        type = self.request.query_params.get('type', None)
        if owner_name:
            queryset = self.search_resource_by_owner_name(owner_name)
        if type:
            queryset = self.queryset.filter(type=type)
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        response_data = serialize(
            'geojson', queryset,
            geometry_field='location_point',
            fields=('user', 'owner_name', 'type')
        )
        response_data = json.loads(response_data)
        response_data["features"] += json.loads(serialize(
                'geojson', queryset,
                geometry_field='location_polygon',
                fields=('user', 'owner_name', 'type')
        ))["features"]

        return Response(response_data)

    def search_resource_by_owner_name(self, owner_name):
        start_match = self.queryset.filter(owner_name__istartswith=owner_name)
        mid_match = self.queryset.filter(owner_name__iregex=f"\s{owner_name}").exclude(owner_name__istartswith=owner_name)
        return list(chain(start_match, mid_match))


class ResourceTypeViewSet(mixins.ListModelMixin, GenericViewSet):

    serializer_class = ResourceTypeSerializer
    queryset = ResourceType.objects.all()
