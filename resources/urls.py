from django.urls import path
from resources import views

urlpatterns = [
    path('map/', views.MapView.as_view(), name="map"),
]