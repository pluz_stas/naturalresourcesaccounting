from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models

from random import randint

from users.managers import UserManager


class User(AbstractUser):
    phone = models.CharField(max_length=16, null=True)
    email = models.EmailField(unique=True)
    username = models.CharField(null=True, blank=True, max_length=1)
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    def __str__(self):
        return self.get_full_name()


class Company(models.Model):
    user = models.ForeignKey(User, related_name="user_companies", on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=16, null=True)
    email = models.EmailField(max_length=50)
    code = models.CharField(max_length=20, blank=True)


class Statement(models.Model):
    SOME_TYPE = 1
    SOME_STATE = 2

    _STATEMENT_TYPES = [
        (SOME_TYPE, "some type")
    ]
    _STATES = [
        (SOME_STATE, "some state")
    ]

    user = models.ForeignKey(User, related_name="user_statements", on_delete=models.CASCADE)
    type = models.IntegerField(choices=_STATEMENT_TYPES)
    text = models.CharField(max_length=255)
    location_point = models.PointField()
    location_polygon = models.PolygonField()
    state = models.IntegerField(choices=_STATES)
    created_date = models.DateTimeField(auto_now_add=True)
    resolved_date = models.DateTimeField(auto_now_add=True)


class Files(models.Model):
    statement = models.ForeignKey(Statement, on_delete=models.CASCADE)
    url = models.CharField(max_length=100)
