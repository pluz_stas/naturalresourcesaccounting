# Generated by Django 3.0.1 on 2020-02-11 09:20

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_db_ext'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statement',
            name='location_point',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326),
        ),
        migrations.AlterField(
            model_name='statement',
            name='location_polygon',
            field=django.contrib.gis.db.models.fields.PolygonField(srid=4326),
        ),
    ]
