import time
from unittest.mock import MagicMock, Mock

from django.test import TestCase

from users.tests.factories import UserFactory
from users.managers import UserManager


class TestUserManager(TestCase):

    def setUp(self):
        self.user = UserFactory()

    def test_create_superuser(self):
        instance = MagicMock()
        mock = Mock()
        mock.side_effect = UserManager.create_superuser(instance, "email", "password")
        self.assertIsNotNone(mock.side_effect.return_value())


class TestUserModel(TestCase):

    def setUp(self):
        self.user = UserFactory()

    def test_str_method(self):
        self.assertEqual(str(self.user), f"{self.user.first_name} {self.user.last_name}")

    def test_get_short_name(self):
        self.assertEqual(str(self.user.get_short_name()), self.user.first_name)

    def test_get_full_name(self):
        self.assertEqual(str(self.user.get_full_name()), '{0} {1}'.format(self.user.first_name, self.user.last_name))

