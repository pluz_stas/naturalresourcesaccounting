from django.urls import include, path
from django.views.generic import TemplateView
from django_registration.backends.activation.views import RegistrationView

from users.forms import UserForm

urlpatterns = [
    path('accounts/register/',
         RegistrationView.as_view(form_class=UserForm,),
         name='django_registration_register',
         ),
    path('accounts/', include('django_registration.backends.activation.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('details/', TemplateView.as_view(template_name="users/details.html"), name="user_details")
]
