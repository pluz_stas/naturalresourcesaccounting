from django.contrib import admin

from users.models import Statement, Company, User, Files

admin.site.register(Statement)
admin.site.register(Company)
admin.site.register(Files)

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "first_name", "last_name", "email")
    exclude = ("password", "user_permissions")